package com.ushakova.tm.command.task;

import com.ushakova.tm.command.AbstractTaskCommand;
import com.ushakova.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Clear all projects.";
    }

    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("***Task Clear***");
        serviceLocator.getTaskService().clear();
    }

    @Override
    @NotNull
    public String name() {
        return "task-clear";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
