package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectUpdateProjectByIdCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Update project by id.";
    }

    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("***Update Project***\nEnter Id:\"");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Project project = serviceLocator.getProjectService().findById(id);
        System.out.println("Enter Project Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter Project Description:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final Project projectUpdated = serviceLocator.getProjectService().updateById(id, name, description, userId);
    }

    @Override
    @NotNull
    public String name() {
        return "project-update-by-id";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
