package com.ushakova.tm.api.service;

import com.ushakova.tm.api.IService;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IUserService extends IService<User> {

    @NotNull
    User add(@Nullable String login, @Nullable String password);

    @NotNull
    User add(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User add(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User findByLogin(@Nullable String login);

    @NotNull
    User lockUserByLogin(@Nullable String login);

    @NotNull
    User removeByLogin(@Nullable String login);

    @NotNull
    User removeUser(@Nullable User user);

    @NotNull
    User setPassword(@Nullable String userId, @Nullable String password);

    @NotNull
    User unlockUserByLogin(@Nullable String login);

    @NotNull
    User updateUser(@Nullable String userId, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

}
